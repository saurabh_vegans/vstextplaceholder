Pod::Spec.new do |spec|
  spec.name         = "TextViewPlaceholder"
  spec.version      = "0.0.1"
  spec.summary      = "A TextViewPlaceholder."

  spec.description  = <<-DESC
    UITextView Placeholder Label
                   DESC

  spec.homepage     = "https://bitbucket.org/saurabh_vegans"
  spec.license      = "MIT"

  spec.author    = "Saurabh"
  spec.source       = { :git => "https://saurabh_vegans@bitbucket.org/saurabh_vegans/vstextplaceholder.git" , :tag => "0.0.1"}

  spec.source_files  = "TextViewPlaceholder", "TextViewPlaceholder/**/*.{h,m,swift}"
  spec.framework  = "UIKit"
  spec.swift_version = "5.0"
  spec.platform     = :ios, "10.0"
end
