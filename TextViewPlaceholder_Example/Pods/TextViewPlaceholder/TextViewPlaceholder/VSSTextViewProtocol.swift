//
//  VSSTextViewProtocol.swift
//  UITextView_Placeholder
//
//  Created by Saurabh on 31/07/19.
//  Copyright © 2019 Vicenza Software Solutions. All rights reserved.
//

import UIKit

public protocol VSSTextViewProtocol {
    func addPlaceholderFor(_ textView: UITextView?)
    func placeholder(for textview: UITextView) -> String?
    func placeholderColor(for textView: UITextView) -> UIColor
}

private let kTextViewPlaceholderTag = 6589

public extension VSSTextViewProtocol where Self: UIViewController {
    private func placeholderLabel(for textView: UITextView) -> UILabel {
        if let label = textView.viewWithTag(kTextViewPlaceholderTag) as? UILabel {
            return label
        }
        
        let placeholderLabel = UILabel()
        placeholderLabel.tag = kTextViewPlaceholderTag
        placeholderLabel.text = placeholder(for: textView)
        placeholderLabel.textColor = placeholderColor(for: textView)
        placeholderLabel.numberOfLines = 0
        placeholderLabel.font = textView.font
        textView.addSubview(placeholderLabel)
        
        placeholderLabel.translatesAutoresizingMaskIntoConstraints = false
        placeholderLabel.isHidden = !textView.text.isEmpty
        
        let leading = NSLayoutConstraint(item: placeholderLabel, attribute: .leading, relatedBy: .equal, toItem: textView, attribute: .leading, multiplier: 1, constant: 5.0)
        let top = NSLayoutConstraint(item: placeholderLabel, attribute: .top, relatedBy: .equal, toItem: textView, attribute: .top, multiplier: 1, constant: 8.0)
        NSLayoutConstraint.activate([leading, top])
        
        return placeholderLabel
    }
    
    private func updatePlaceholder(for textView: UITextView) {
        let placeholder = placeholderLabel(for: textView)
        placeholder.isHidden = !textView.text.isEmpty
    }
    
    func addPlaceholderFor(_ textView: UITextView?) {
        if let textView = textView { updatePlaceholder(for: textView) }
        NotificationCenter.default.addObserver(forName: UITextView.textDidChangeNotification, object: textView, queue: .main) { [unowned self] notification in
            if let textView = notification.object as? UITextView {
                self.updatePlaceholder(for: textView)
            }
        }
    }
    
    func placeholderColor(for textView: UITextView) -> UIColor {
        return UIColor.lightGray
    }
    
    func placeholder(for textview: UITextView) -> String? {
        return "Please enter text"
    }
}
