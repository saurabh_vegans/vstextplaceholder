//
//  ViewController.swift
//  TextViewPlaceholder_Example
//
//  Created by Saurabh on 02/09/19.
//  Copyright © 2019 Vicenza Software Solutions. All rights reserved.
//

import UIKit
import TextViewPlaceholder

class ViewController: UIViewController, VSSTextViewProtocol {
    @IBOutlet weak var textView: UITextView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textView?.layer.borderWidth = 1.0
        textView?.layer.borderColor = UIColor.black.cgColor
        
        addPlaceholderFor(textView)
    }
}

